# Release notes

## Version v1.2.0
* Introduce more flexible filtering definitions
* Changes to standardization
  * Ensure minimal starting position is base 1
  * Add `CN` from Canvas to the `FORMAT` field when Canvas is merged with Manta

## Version v1.1.0
* Rescue ACMG_class 4,5 and add ACMG_class column to TSV output

## Version v1.0.0
### Changes
Breaking change
* Removes the `--print`-argument from the Python scripts

New features
* Directly installs runnable scripts
  * `sv_standardizer`
  * `sv_postprocessing`
  * `sv_wgs_filtering`
* Moves parsing and formatting of VCFs into `src/sv_utils.py`
* Remove non-variants (`DRAGEN:REF`)
* Add INFO fields `canvasSM`, `canvasPE`, `canvasBC` to back up FORMAT fields `SM`, `PE`, `BC`. Useful for merging.

## Version v0.9.0
Version `v0.9.0` of this repository was never released, but is a collection of the Python scripts hardcoded in the following repositories
* `svtools@v0.1.2`
* `anno-targets@74a1b0f8a4f7045b8e8a628d059c2c0927e558d3`
