# Tools for postprocessing of structural variants
## Standardizing a VCF file from our CNV-callers
The runnable script `sv_standardizer` will print a standardized VCF to `stdout` when executed as
```
sv_standardizer VCF_FILENAME --caller CALLER_NAME --sample SAMPLE_NAMES
```
where 
* `VCF_FILENAME` is the VCF from one of the callers `["manta", "delly", "tiddit", "cnvnator", "svaba", "canvas"]`
  * If no filename is given, the function will take input from `stdin`
* `CALLER_NAME` explicitly sets the source of the `VCF_FILENAME`
  * `CALLER_NAME` can be omitted if the `VCF_FILENAME` starts with `{CALLER_NAME}_`
* `SAMPLE_NAMES` is a comma-separated list of SAMPLE column names to ensure equal naming for all callers
  * It is expected that the order of SAMPLE columns is the same for all callers

## Postprocessing of the FILTER column
The FILTER column can be updated when a variant record fails a quality test.
Postprocessing is only needed in special cases, e.g. on output from `CNVnator` and `Delly`.

The runnable script `sv_postprocessing` will print a quality filtered VCF to `stdout` when executed as
```
sv_postprocessing STANDARDIZED_MERGED_VCF --set-quality-filters WGS_SVPARAMS_FILTER --callers CALLERS
```
where
* `STANDARDIZED_MERGED_VCF` has been standardized by `sv_standardizer` and merged by [SVDB](https://github.com/J35P312/SVDB)
  * If no filename is given, the function will take input from `stdin`
* `WGS_SVPARAMS_FILTER` is a JSON-formatted string defined in [analysistypeconfig.schema.json](https://gitlab.com/ousamg/apps/anno-targets/-/blob/bcfb63cce918e31086e593a90baba6f234523d8c/config/analysistypeconfig.schema.json) under `WGS.svparams.filter`
* `CALLERS` is a list of callers that were used when making the merged VCF

## Filtering based on frequency or selecting a interpretation group
Filtering means to remove records from the VCF based on certain conditions 
1. remove variants with high frequencies (set frequency filters).
   Exceptions are allowed (set rescue filters)
2. return a subset of variants (set interpretation group)
3. in debug mode, high frequency variants are not
   removed, but frequency tags are added to the FILTER
   column based on frequencies (set frequency filters)

```
sv_wgs_filtering STANDARDIZED_MERGED_VCF \
  --output-format OUTPUT_FORMAT \
  --set-interpretation-group SET_INTERPRETATION_GROUP \
  --set-frequency-filters SET_FREQUENCY_FILTERS \
  --set-rescue-filters SET_RESCUE_FILTERS \
  --caller-priority CALLER_PRIORITY \
```
where
* `STANDARDIZED_MERGED_VCF` has been standardized by `sv_standardizer`
  * If no filename is given, the function will take input from `stdin`
  * Optional: `STANDARDIZED_MERGED_VCF` may also have been postprocessed by `sv_postprocessing`
* `OUTPUT-FORMAT` is one of `vcf` or `tsv` or `bed`
* `SET_INTERPRETATION_GROUP` is a JSON-formatted string defined in [analysistypeconfig.schema.json](https://gitlab.com/ousamg/apps/anno-targets/-/blob/a00a1c6dd548bcaf8b757c4310a221611940465c/config/analysistypeconfig.schema.json) under `svparams.interpretation_groups`
  * The interpretation group is applied prior to frequency filtering
* `SET_FREQUENCY_FILTERS` is a JSON-formatted string defined in [analysistypeconfig.schema.json](https://gitlab.com/ousamg/apps/anno-targets/-/blob/a00a1c6dd548bcaf8b757c4310a221611940465c/config/analysistypeconfig.schema.json) under `svdb.criteria`
  * Note that frequency filtering by the Gnomad database is defined as "common" filtering, whereas all other INDB and SweGen filtering is caller specific
* `SET_RESQUE_FILTERS` is a JSON-formatted string defined in [analysistypeconfig.schema.json](https://gitlab.com/ousamg/apps/anno-targets/-/blob/a00a1c6dd548bcaf8b757c4310a221611940465c/config/analysistypeconfig.schema.json) under `svdb.exceptions`
* `CALLER_PRIORITY` is a list of callers in order of priority. The freqency database for the caller of highest priority will be used for filtering when multiple callers have been merged
* Optional: `--debug` will show debug information and cause `SET_FREQUENCY_FILTERS` to be applied as annotations and not filtered

## Getting access to this repo [from version v1.0.0]
This is a private repo and can be installed by
```
pip install git+https://${GITLAB_TOKEN_USER}:${GITLAB_TOKEN}@gitlab.com/ousamg/apps/sv-pack.git@${VERSION}#egg=svpack
```
as described in
https://docs.readthedocs.io/en/stable/guides/private-python-packages.html#gitlab

`GITLAB_TOKEN_USER` and `GITLAB_TOKEN` are specific to this repo and have been created by a `Maintainer` as described in
https://docs.gitlab.com/ee/user/project/deploy_tokens/#creating-a-deploy-token

`VERSION` can be any `tag`, `branch` or `commit` as described in
https://pip.pypa.io/en/stable/reference/pip_install/#git

_Note: Gitlab-tokens can change independently of the version of this repo. All valid Gitlab tokens are listed in the project's Wiki on GitLab._

