#!/usr/bin/env python

"""sv_standardizer.py: Script that removes errors from VCF
                       files originating from structural variant callers.
                       Error correction is minimal, just to
                       prevent downstream tools from failing.
                       In cases when downstream tools are not following
                       VCF standards, this function may uglify
                       the VCF representation. Caution is adviced.
                       When tools in the pipeline require
                       mutually exclusive VCF representations, several runs of
                       this function are required.
                       For this reason, this function has to be available
                       in basepipe prior to merging and database generation,
                       and in anno-targets prior to Ella.
"""

import argparse
import json
import sys
import logging
import itertools as it
from collections import OrderedDict

from svpack import sv_utils as utils

_log = logging.getLogger(__name__)


#################
# Known callers #
#################
CALLERS = ["manta", "delly", "tiddit",
           "cnvnator", "svaba", "canvas"]


############################
# Parse command line input #
############################
def input_parser():
    parser = argparse.ArgumentParser(description='Standardize VCF')
    parser.add_argument('file', nargs='?', type=argparse.FileType('r'),
                        default=sys.stdin, help='VCF filename')
    parser.add_argument('--caller', type=str,
                        help='Caller used to create VCF file. If not provided: infer from filename. Assumes filename starts with "CALLER_"')
    parser.add_argument('--sample', type=str,
                        help='Sample name(s) (one per column, comma separated). Used for renaming sample columns')
    return parser.parse_args()


#############################
# Standardization functions #
#############################

######################################
# Additional standardization headers #
######################################
def add_headers_standardize(caller,
                            known_callers):
    """
    Define the header lines to add in standardize
    """
    caller_id = "{}ID".format(caller.upper())
    id_info = dict(ID=caller_id,
                   Number=1,
                   Type="String",
                   Description='"%s ID value for variant"' % caller.upper())
    canvas_sm = dict(ID="canvasSM",
                     Number=".",
                     Type="Float",
                     Description='"All observed linear copy ratios of the segment mean for record"')
    canvas_pe = dict(ID="canvasPE",
                     Number=".",
                     Type="Integer",
                     Description='"All observed numbers of improperly paired end reads at start and stop breakpoints for record"')
    canvas_bc = dict(ID="canvasBC",
                     Number=".",
                     Type="Integer",
                     Description='"All observed number of bins in the region for record"')
    canvas_cn = dict(ID="canvasCN",
                     Number="1",
                     Type="Integer",
                     Description='"Estimated copy number"')
    hom_ref = dict(ID="HomRef",
                   Description='"homozygous reference call (filter applied at sample level)"')

    if caller in known_callers:
        if caller == "delly":
            return [
                ("##INFO=<ID="+caller_id, id_info),
                ("##FILTER=<ID=HomRef", hom_ref)
            ]
        if caller == "canvas":
            return [
                ("##INFO=<ID="+caller_id, id_info),
                ("##INFO=<ID=canvasSM", canvas_sm),
                ("##INFO=<ID=canvasPE", canvas_pe),
                ("##INFO=<ID=canvasBC", canvas_bc),
                ("##INFO=<ID=canvasCN", canvas_cn)
            ]
        else:
            return [
                ("##INFO=<ID="+caller_id, id_info),
            ]
    else:
        return []


###########################################
# Fixing known collisions between headers #
###########################################
def _standardize_sample_names(header_record,
                              sample_names):
    """
    Set sample name in header when provided by user
    """
    assert len(header_record["SAMPLES"]) == len(sample_names), f"Expected one name per sample column in VCF, got {sample_names}"
    header_record["SAMPLES"] = sample_names
    return header_record


def _standardize_header_info(header_record):
    """
    Standardize conflicting header INFO definition
    """
    if header_record["ID"] == "SVLEN":
        # For CNVnator
        header_record["Number"] = "."
        header_record["Description"] = '"Difference in length between REF and ALT alleles"'
        return header_record
    elif header_record["ID"] == "HOMLEN":
        # For Delly
        header_record["Number"] = "."
        return header_record
    elif header_record["ID"] == "END":
        # For SVDB --export database to VCF
        header_record["Type"] = "Integer"
        return header_record
    else:
        return header_record


def _standardize_header_format(header_record):
    """
    Standardize conflicting header FORMAT definition
    """
    if header_record["ID"] == "DR":
        # For Delly and Tiddit
        header_record["Number"] = "."
        return header_record
    elif header_record["ID"] == "RR":
        # For Delly and Tiddit
        header_record["Number"] = "."
        return header_record
    elif header_record["ID"] == "PE":
        # For CNVnator. FORMAT=PE is unused but collides with definition for Canvas. Redefine.
        header_record["Number"] = "2"
        header_record["Description"] = '"Number of improperly paired end reads at start and stop breakpoints"'
        return header_record
    else:
        return header_record


def _standardize_header_record(header_type,
                               header_record,
                               sample_names=[]):
    """
    Standardize header record
    """
    if header_type.startswith("##INFO"):
        return (header_type, _standardize_header_info(header_record))
    if header_type.startswith("##FORMAT"):
        return (header_type, _standardize_header_format(header_record))
    elif all([header_type.startswith("#CHROM"),
              sample_names]):
        return (header_type, _standardize_sample_names(header_record, sample_names))
    else:
        return (header_type, header_record)


def standardize_header(header,
                       sample_names):
    """
    Standardize the VCF header lines
    """
    return map(
        lambda h: _standardize_header_record(*utils.parse_header(h), sample_names=sample_names), header
    )


################################################
# VCF record standardizer (internal functions) #
################################################
def _get_standardized_svlen(record):
    """
    Get SVLEN from variant record
    Standardize if necessary
    * Create for non-INS Delly variants
    * Create for Manta INS and BND (legacy)
    * Create for Tiddit BND (legacy)
    * Ensure negative SVLEN for all DEL
    * (Beware!) BND gets SVLEN=-1 (Gnomad convension)
    """

    if record["info"].get("SVLEN"):
        SVLEN = int(record["info"]["SVLEN"])
    elif record["info"]["SVTYPE"] in ["DEL", "DUP", "INV", "CNV"]:
        if "END" in record["info"]:
            SVLEN = int(record["info"]["END"]) - int(record["pos"])
        elif not record["alt"].startswith("<"):
            SVLEN = len(record["alt"]) - len(record["ref"])
        else:
            _log.warning("No rule for defining SVLEN for %s" % record["id"])
            SVLEN = 0
    elif record["info"]["SVTYPE"] == "INS":
        if record["info"].get("INSLEN"):
            # For older versions of Delly
            SVLEN = int(record["info"]["INSLEN"])
        elif record["info"].get("SVINSLEN"):
            # Manta specific
            SVLEN = int(record["info"]["SVINSLEN"])
        elif not record["alt"].startswith("<"):
            SVLEN = len(record["alt"])-len(record["ref"])
        else:
            SVLEN = 0
    elif record["info"]["SVTYPE"] == "BND":
        # Manta specific
        SVLEN = int(record["info"].get("SVINSLEN", 0))
    else:
        _log.warning("No rule for defining SVLEN for %s" % record["id"])
        SVLEN = 0

    # SVLEN = -1 for unknown length or BND
    # Due to Gnomad convension
    if not SVLEN:
        SVLEN = -1

    # Remove END for Delly BND
    if all([record["info"]["SVTYPE"] == "BND",
            "END" in record["info"]]):
        record["info"].pop("END")

    # For all: Ensure DEL has negative length
    if record["info"]["SVTYPE"] == "DEL":
        SVLEN = -abs(int(SVLEN))
    return SVLEN


def _get_standardized_gt(record,
                         sample,
                         caller):
    """
    Ella expects genotype
    * DUP: './.'
    """
    # For Ella:
    # Has to be done after merging of callers
    # Otherwise SVDB will not include them in database
    if record["alt"] == "<DUP>":
        GT = "./."
    else:
        GT = sample["GT"]
    return GT


def _standardize_record(record,
                        samples,
                        caller,
                        known_callers=[]):
    """
    Standardize variant line
    * Tiddit: ALT=TDUP -> DUP:TANDEM
    * Delly: ALT=DUP -> DUP:TANDEM (only tandem dups)
    * Canvas: SVTYPE=CNV -> DEL or DUP as appropriate
    * Merged: Add "CN" format field if "canvasCN" exists

    All:
    * add SVLEN if missing (for DEL: SVLEN<0)
    * add caller specific INFO field with ID
      (to keep track when merging)
    """

    # For Tiddit
    if record["alt"] == "<TDUP>":
        record["alt"] = "<DUP:TANDEM>"
    if record["info"]["SVTYPE"] == "TDUP":
        record["info"]["SVTYPE"] = "DUP"
    # For Delly
    if all([caller == "delly",
            record["info"]["SVTYPE"] == "DUP"]):
        record["alt"] = "<DUP:TANDEM>"
    # For Canvas
    if record["info"]["SVTYPE"] == "CNV":
        record["info"]["SVTYPE"] = record["alt"].strip("<>")
        assert record["info"]["SVTYPE"].startswith(("DEL", "DUP"))
    if caller == "canvas":
        # Move evidence for variant to INFO. Used to estimate CN
        # when Manta and Canvas are merged
        canvasSM = ",".join(map(lambda s: s["SM"], samples))
        canvasPE = ",".join(map(lambda s: s["PE"], samples))
        canvasBC = ",".join(map(lambda s: s["BC"], samples))
        canvasCN = ",".join(map(lambda s: s["CN"], samples))
        record["info"]["canvasSM"] = canvasSM
        record["info"]["canvasPE"] = canvasPE
        record["info"]["canvasBC"] = canvasBC
        record["info"]["canvasCN"] = canvasCN

    # Canvas (at least) violates VCF 1-index
    if record["pos"] == "0":
        record["pos"] = "1"

    # For all
    record["info"]["SVLEN"] = _get_standardized_svlen(record)

    # For manta, delly, tiddit, cnvnator, svaba, canvas
    if caller in known_callers:
        caller_id = "{caller}ID".format(caller=caller.upper())
        record["info"][caller_id] = record["id"]

    # Add CN to FORMAT when Canvas is in the merged file
    if all([caller == "merged",
            "canvasCN" in record["info"]]):
        format_keys = OrderedDict.fromkeys(record["format"].split(":"))
        format_keys["CN"] = None
        record["format"] = ":".join(format_keys.keys())

    return record


def _standardize_sample(record,
                        sample,
                        caller):
    """
    Standardize sample specific FORMAT values
    * CNVnator: ensure CN = round(2*natorRD)

    Merged file:
    * Ensure GT on Ella format
    """
    if caller == "cnvnator":
        sample["CN"] = "{:.0f}".format(round(2*float(record["info"]["natorRD"])))

    if caller == "merged":
        sample["GT"] = _get_standardized_gt(record, sample, caller)
        if "canvasCN" in record["info"]:
            sample["CN"] = record["info"]["canvasCN"]

    return sample


def _get_standardized_filter(record,
                             sample):
    """
    Standardize and return the FILTER column
    * Delly:
      add HomRef if GT=0/0
    """

    filters = OrderedDict.fromkeys(record["filter"].split(";"))

    gt = sample["GT"]
    if gt in ["0/0"]:
        filters.setdefault("HomRef")

    if all([len(filters) > 1,
            "PASS" in filters]):
        filters.pop("PASS")

    FILTER = ";".join(filters)

    return FILTER


def _standardize(record,
                 samples,
                 caller="manta",
                 known_callers=[]):
    """
    Standardize variant record
    """
    record_std = _standardize_record(record,
                                     samples,
                                     caller,
                                     known_callers=known_callers)

    filters_std = map(lambda s: _get_standardized_filter(record_std, s),
                      samples)
    record_std["filter"] = ";".join(filters_std)

    samples_std = map(lambda s: _standardize_sample(record_std, s, caller),
                      samples)

    return (record_std, samples_std)


######################################################
# VCF record standardizer function and main function #
######################################################
def standardize(line,
                caller,
                known_callers):
    """
    Standardize VCF variant line
    * Standardize if line contains a variant
    * Return empty string if line does not contain a variant
    """
    (record, samples) = utils.parse_record(line)
    # All variants must have a SVTYPE
    # Canvas also report non-variants, that must be removed
    if record["info"].get("SVTYPE"):
        (record_std, samples_std) = _standardize(record,
                                                 samples,
                                                 caller,
                                                 known_callers)

        return utils.format_record(record_std, samples_std)
    else:
        return ""


def standardize_header_body(is_header,
                            records,
                            sample_names,
                            caller,
                            known_callers):
    """
    Standardize each record as either
    * header line
    or
    * body line
    """
    if is_header:
        return utils.complement_header(standardize_header(records, sample_names),
                                       add_headers_standardize(caller, known_callers))
    else:
        return filter(None, map(lambda line: standardize(line, caller, known_callers),
                      records))


def do_sv_standardizing(file_object,
                        sample_names,
                        caller,
                        known_callers=[]):
    """
    Run run_function:
    * Standardize VCF format
    """
    f_split = it.groupby(file_object, key=lambda line: line.startswith("#"))
    output_it = it.starmap(lambda is_header, records:
                           standardize_header_body(is_header, records, sample_names, caller, known_callers),
                           f_split)

    return it.chain.from_iterable(output_it)


def main():
    logging.basicConfig(level=logging.INFO)

    args = input_parser()
    _log.info(f"Run parameters:\n{json.dumps(vars(args), indent=2, cls=utils.JSONArgparseEncoder)}")

    if args.caller:
        caller = args.caller.lower()
    else:
        # Assume filename starts with '${CALLER}_'
        caller = args.file.name.rsplit("/", 1)[-1].split("_", 1)[0].lower()
        _log.info(f"Assuming caller: {caller} (because filename starts with {caller}_)")

    if args.sample:
        sample_names = args.sample.split(",")
    else:
        sample_names = []
        _log.info("Using sample names from the VCF file")

    if caller == "merged":
        _log.warning("Merged file for Ella. <DUP> genotype will be set to './.'")
    elif caller not in CALLERS:
        _log.warning("Caller unknown: " + caller + " is not one of " + ",".join(CALLERS))

    with args.file:
        output = do_sv_standardizing(args.file,
                                     sample_names,
                                     caller,
                                     known_callers=CALLERS)
        print(*output, sep="\n")


if __name__ == "__main__":
    main()
