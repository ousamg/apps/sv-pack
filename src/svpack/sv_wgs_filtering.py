#!/usr/bin/env python

"""sv_wgs_filtering.py: Function that can
                        1) remove variants based on high frequency.
                           Frequency limits are defined in `analysistypeconfig.json`
                           in `anno-targets`. In the same file one can set rescue filters
                           for variants that should be kept despite high frequency
                        2) return a subset of variants based on variant
                           length and type (interpretation groups). The interpretation group
                           is applied before frequency filtering
                        3) in debug mode, high frequency variants are not
                           removed, but frequency tags are added to the FILTER
                           column based on frequencies in the INFO fields
                           and frequency limits in `analysistypeconfig.json` in
                           `anno-targets`
"""

import sys
import argparse
import json
import ast
import logging
import itertools as it

from collections import OrderedDict

from svpack import sv_utils as utils

_log = logging.getLogger(__name__)


def input_parser():
    parser = argparse.ArgumentParser(description='Postprocessing of annotated VCF')
    parser.add_argument('file', nargs='?', type=argparse.FileType('r'),
                        default=sys.stdin, help='VCF filename')
    parser.add_argument('--output-format', type=str, default='vcf',
                        help="Output format (default: %(default)s)")
    parser.add_argument('--set-interpretation-group', type=str, default="{}",
                        help='JSON-formatted string to override: %(default)s')
    parser.add_argument('--set-frequency-filters', type=str, default="{}",
                        help='JSON-formatted string to override: %(default)s')
    parser.add_argument('--set-rescue-filters', type=str, default='{}',
                        help='JSON-formatted string to override: %(default)s')
    parser.add_argument('--caller-priority', type=str, nargs="+",
                        default=["manta", "canvas", "tiddit", "delly", "cnvnator"],
                        help='JSON-formatted string to override: %(default)s')
    parser.add_argument('--debug', action='store_true',
                        help="Get output on debug form")
    return parser.parse_args()


HEADER = [
    "CHR", "POS", "END", "ID", "SVTYPE", "SVLEN", "FILTER", "COOR",
    "OCC_INDB_MANTA", "OCC_INDB_DELLY", "OCC_INDB_TIDDIT",
    "OCC_INDB_CNVNATOR", "OCC_INDB_CANVAS",
    "FRQ_SWEGEN_MANTA", "FRQ_SWEGEN_DELLY", "FRQ_SWEGEN_TIDDIT", "FRQ_SWEGEN_CNVNATOR",
    "FRQ_GNOMAD",
    "QUAL", "IMPRECISE", "EVENT", "SR", "PR", "RD", "EVIDENCE", "Q0",
    "MANTA", "DELLY", "TIDDIT", "CNVNATOR", "CANVAS", "CN", "GT", "ACMG_class"
]


############################################################
# Select subset of variants based on INTERPRETATION GROUPS #
############################################################
def _get_csq_field(csq,
                   field_number):
    """
    Get iterator of annotations in CSQ 'field_number'
    """
    vep_annotations = csq.split(",")
    return list(map(lambda s: s.split("|").pop(field_number), vep_annotations))


def select_for_interpretation(record,
                              samples,
                              group={}):
    """
    Determine whether to include this record from interpretation
    I.e. keep variants that return True
    """
    if utils.check_filter_relations(record, samples, group):
        return True
    else:
        return False


##############################################
# Get frequency and do filtering based on it #
##############################################
def _has_called(caller, record):
    """
    Return True if caller has called
    Identified by presence of CALLERID (set by sv_standardizer)
    """
    caller_id = f"{caller.upper()}ID"
    return caller_id in record["info"]


def get_priority_caller(record, caller_priority):
    """
    Get prioritized caller
    """
    has_called = list(
        filter(lambda c: _has_called(c, record), caller_priority)
        )
    if has_called:
        return has_called[0]
    else:
        return None


# Remove hi freq variants directly
def frequency_filter(record,
                     samples,
                     filters={},
                     exceptions={},
                     caller_priority=[]):
    """
    Return variants with low frequency in population databases

    filters = {
        caller: {
            database_name: {filters: ...}
            }
        "common": {
            database_name: {filters: ...}
            }
    }

    exceptions = {
        exception_name: {filters: ...}
    }
    """
    priority_caller = get_priority_caller(record, caller_priority)
    priority_filters = filters.get(priority_caller, {})
    common_filters = filters.get("common", {})

    if utils.filter_body(record, samples, exceptions, group_names=[]):
        return True
    elif utils.filter_body(record, samples, priority_filters, group_names=[]):
        return False
    elif utils.filter_body(record, samples, common_filters, group_names=[]):
        return False
    else:
        return True


##################################################################################
# Optionally, if not removing all high-frequency variants, add new FILTER to VCF #
##################################################################################
def add_headers():
    """
    Add header lines when introducing new fields in VCF
    """
    hi_freq_indb = dict(ID="HiFreqInHdb",
                        Description='"High frequency in inhouse SVDB database"')
    hi_freq_swegen = dict(ID="HiFreqSwegen",
                          Description='"High frequency in SweGen SVDB database"')
    hi_freq_gnomad = dict(ID="HiFreqGnomad",
                          Description='"High frequency in Gnomad database"')
    segdup = dict(ID="SEGDUP",
                  Description='"Overlapping segdup region by more than 70% of SVLEN"')

    return [
        ("##FILTER=<ID=HiFreqInHdb", hi_freq_indb),
        ("##FILTER=<ID=HiFreqSwegen", hi_freq_swegen),
        ("##FILTER=<ID=HiFreqGnomad", hi_freq_gnomad),
        ("##FILTER=<ID=SEGDUP", segdup),
    ]


def complement_header(header,
                      added_headers):
    """
    Parse header.
    Optionally add new header definitions to output
    """
    parsed_header = map(utils.parse_header, header)
    return utils.complement_header(parsed_header,
                                   added_headers)


# Mark hi freq variants for manual inspection
def add_frequency_filter(record,
                         filters,
                         caller_priority=[]):
    """
    Add filter to FILTER column if  with high frequency in databases
    * frequency filters
    """

    priority_caller = get_priority_caller(record, caller_priority)
    priority_filters_indb = filters.get(priority_caller, {}).get("indb", {})
    priority_filters_swegen = filters.get(priority_caller, {}).get("swegen", {})
    filters_gnomad = filters.get("common", {}).get("gnomad", {})

    new_filters = OrderedDict.fromkeys(record["filter"].split(";"))

    if utils.check_filter_relations(record, [], priority_filters_indb):
        new_filters.setdefault("HiFreqInHdb")

    if utils.check_filter_relations(record, [], priority_filters_swegen):
        new_filters.setdefault("HiFreqSwegen")

    if utils.check_filter_relations(record, [], filters_gnomad):
        new_filters.setdefault("HiFreqGnomad")

    if all([len(new_filters) > 1,
            "PASS" in new_filters]):
        new_filters.pop("PASS")

    FILTER = ";".join(new_filters)

    return FILTER


def mark_record_for_filtering(record,
                              samples,
                              filters,
                              caller_priority):
    """
    * Update FILTER column to indicate high frequency
    """
    record["filter"] = add_frequency_filter(record,
                                            filters=filters,
                                            caller_priority=caller_priority)
    return (record, samples)


#############################################################
# Functions for formatting output. Notably Excel compatible #
#############################################################
def _format_end(svtype,
                pos,
                end,
                svlen):
    """
    Set END position

    The END is used by interval-based tools such as bedtools

    END is not defined for INS or BND.
    Function calculates an virtual END based on SVLEN when possible
    """
    if svtype in ["INS", "BND"]:
        if int(svlen) > 0:
            return str(int(pos) + abs(int(svlen)))
        else:
            return pos
    else:
        return end


def _format_imprecise(info,
                      imprecise_limit=20):
    """
    Set the IMPRECISE flag

    Base flag on CIPOS or CIEND if not set by caller
    """

    if "IMPRECISE" in info:
        return True
    elif "PRECISE" in info:
        return False
    else:
        CIPOS = max(map(abs, map(int, info.get("CIPOS", "-10000,0").split(","))))
        CIEND = max(map(abs, map(int, info.get("CIEND", "-10000,0").split(","))))
        if max(CIEND, CIPOS) > imprecise_limit:
            return True
        else:
            return False


def format_evidence(record,
                    samples):
    """
    Evidence is based on
    * discordant reads (PR)
    * split reads (SR)
    * read depth (RD)

    Based on the Anna Lindstrand et al. paper, we assume that discordant reads
    are the main EVIDENCE for break-end callers
    """

    if record["info"].get("MANTAID"):
        SR = ";".join(map(lambda sample: sample.get("SR", ""), samples))
        PR = ";".join(map(lambda sample: sample["PR"], samples))
        if "natorRD" in record["info"]:
            RD = record["info"]["natorRD"]
        elif "canvasSM" in record["info"]:
            RD = record["info"]["canvasSM"]
        else:
            RD = None
        EVIDENCE = PR.split(",")[-1]
        GT = ";".join(map(lambda sample: sample["GT"], samples))
        return (SR, PR, RD, EVIDENCE, None, GT)
    elif record["info"].get("TIDDITID"):
        SR = ";".join(
            map(lambda sample: "(" + sample['RR'] + ")," + sample['RV'], samples)
        )
        PR = ";".join(
            map(lambda sample: "(" + sample['DR'] + ")," + sample['DV'], samples)
        )
        if record["info"]["SVTYPE"] != "BND":
            RD = "{:.4f}".format(float(record["info"]["COVM"])/(float(record["info"]["COVA"])+float(record["info"]["COVB"])+1))
        else:
            RD = "-1"
        EVIDENCE = ";".join(map(lambda sample: sample["DV"], samples))
        CN = ";".join(map(lambda sample: sample["CN"], samples))
        GT = ";".join(map(lambda sample: sample["GT"], samples))
        return (SR, PR, RD, EVIDENCE, CN, GT)
    elif record["info"].get("DELLYID"):
        SR = ";".join(map(lambda sample: sample["RR"] + "," + sample["RV"], samples))
        PR = ";".join(map(lambda sample: sample["DR"] + "," + sample["DV"], samples))
        RD = ";".join(
            map(lambda rd: "{:.4f}".format(rd),
                map(lambda sample: float(sample["RC"])/(float(sample["RCL"])+float(sample["RCR"])+1), samples)
                )
        )
        EVIDENCE = ";".join(map(lambda sample: sample["DV"], samples))
        CN = ";".join(map(lambda sample: sample["CN"], samples))
        GT = ";".join(map(lambda sample: sample["GT"], samples))
        return (SR, PR, RD, EVIDENCE, CN, GT)
    elif record["info"].get("CNVNATORID"):
        RD = record["info"]["natorRD"]
        CN = ";".join(map(lambda sample: sample["CN"], samples))
        GT = ";".join(map(lambda sample: sample["GT"], samples))
        return (None, None, RD, None, CN, GT)
    elif record["info"].get("CANVASID"):
        RD = ";".join(map(lambda sample: sample["SM"], samples))
        PR = ";".join(map(lambda sample: sample["PE"], samples))
        CN = ";".join(map(lambda sample: sample["CN"], samples))
        GT = ";".join(map(lambda sample: sample["GT"], samples))
        return (None, PR, RD, None, CN, GT)
    else:
        _log.warning("No evidence for variant")
        return (None, None, None, None, None, None)


def format_record_tsv(record,
                      samples):
    """
    Tab-separated format suitable for Excel, TSV and BED-files
    """

    SVTYPE = record["info"]["SVTYPE"]
    SVLEN = record["info"]["SVLEN"]

    END = _format_end(SVTYPE, record["pos"], record["info"].get("END", -1), SVLEN)

    IMPRECISE = _format_imprecise(record["info"])

    (SR, PR, RD, EVIDENCE, CN, GT) = format_evidence(record, list(samples))

    tsv_record = {
        "CHR": record["chrom"],
        "POS": record["pos"],
        "END": END,
        "ID": record["id"].split("|")[0],  # .rsplit(":", 1)[0], # last part when not --notags for SVDB
        "SVTYPE": SVTYPE,
        "SVLEN": abs(int(SVLEN)),
        "FILTER": record["filter"],
        "COOR": record["chrom"] + ":" + record["pos"] + "-" + END,
        "OCC_INDB_MANTA": record["info"].get("OCC_INDB_MANTA", 0),
        "OCC_INDB_DELLY": record["info"].get("OCC_INDB_DELLY", 0),
        "OCC_INDB_TIDDIT": record["info"].get("OCC_INDB_TIDDIT", 0),
        "OCC_INDB_CNVNATOR": record["info"].get("OCC_INDB_CNVNATOR", 0),
        "OCC_INDB_CANVAS": record["info"].get("OCC_INDB_CANVAS", 0),
        "FRQ_SWEGEN_MANTA": record["info"].get("FRQ_SWEGEN_MANTA", 0),
        "FRQ_SWEGEN_DELLY": record["info"].get("FRQ_SWEGEN_DELLY", 0),
        "FRQ_SWEGEN_TIDDIT": record["info"].get("FRQ_SWEGEN_TIDDIT", 0),
        "FRQ_SWEGEN_CNVNATOR": record["info"].get("FRQ_SWEGEN_CNVNATOR", 0),
        "FRQ_GNOMAD": record["info"].get("FRQ_GNOMAD", 0),
        "QUAL": record["qual"],
        "IMPRECISE": "IMPRECISE" if IMPRECISE else "PRECISE",
        "EVENT": record["info"].get("EVENT", None),
        "SR": SR,
        "PR": PR,
        "RD": RD,
        "EVIDENCE": EVIDENCE,
        "Q0": record["info"].get("natorQ0", None),
        "MANTA": record["info"].get("MANTAID"),
        "DELLY": record["info"].get("DELLYID"),
        "TIDDIT": record["info"].get("TIDDITID"),
        "CNVNATOR": record["info"].get("CNVNATORID"),
        "CANVAS": record["info"].get("CANVASID"),
        "CN": CN,
        "GT": GT,
        "ACMG_class": record["info"].get("ACMG_class", None)
    }

    return "\t".join(map(lambda header: str(tsv_record[header]), HEADER))


def filter_header_body(is_header,
                       records,
                       frequency_filters,
                       rescue_filters,
                       interpretation_group,
                       caller_priority,
                       output_format,
                       debug):
    """
    Filter either header lines or body lines
    """
    if is_header:
        if output_format.lower() in ["tsv", "bed"]:
            return ["#{}".format("\t".join(HEADER))]
        else:
            return complement_header(records,
                                     add_headers())
    else:
        # Make iterator 'parsed_body'
        parsed_body = map(utils.parse_record, records)

        # Limit interpretation to interpretation group
        if interpretation_group:
            # Overwrite 'parsed_body'
            parsed_body = filter(
                lambda rs: select_for_interpretation(*rs, group=interpretation_group), parsed_body
            )

        # Mark variants if they are high frequency
        if frequency_filters:
            # Overwrite 'parsed_body'
            parsed_body = map(
                lambda rs: mark_record_for_filtering(*rs,
                                                     frequency_filters,
                                                     caller_priority=caller_priority), parsed_body
            )

        # Remove variants if they are high frequency
        if frequency_filters and not debug:
            # Overwrite 'parsed_body'
            parsed_body = filter(
                lambda rs: frequency_filter(*rs,
                                            filters=frequency_filters,
                                            exceptions=rescue_filters,
                                            caller_priority=caller_priority), parsed_body
            )

        if output_format.lower() in ["tsv", "bed"]:
            return it.starmap(format_record_tsv, parsed_body)
        else:
            return it.starmap(utils.format_record, parsed_body)


def do_sv_wgs_filtering(file_object,
                        frequency_filters={},
                        rescue_filters={},
                        interpretation_group={},
                        caller_priority=[],
                        output_format="vcf",
                        debug=False):
    """
    * Filter input
    * Format output
    """
    # Split vcf file_object iterator into
    # [('True', header), ('False', body)]
    f_split = it.groupby(file_object, key=lambda line: line.startswith("#"))

    # Processing ('True', header) and ('False', body)
    output_it = map(lambda f_group:
                    filter_header_body(*f_group,
                                       frequency_filters,
                                       rescue_filters,
                                       interpretation_group,
                                       caller_priority,
                                       output_format,
                                       debug),
                    f_split)

    # Flatten [ header_post, filtered_body ] into iterator
    return it.chain.from_iterable(output_it)


def main():
    args = input_parser()
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    _log.info(f"Run parameters:\n{json.dumps(vars(args), indent=2, cls=utils.JSONArgparseEncoder)}")

    # Using ast.literal_eval for JSON-style strings, because Bash-variables
    # are often written on form  "{'key': item}" and not '{"key": item}'
    interpretation_group = ast.literal_eval(args.set_interpretation_group)

    frequency_filters = ast.literal_eval(args.set_frequency_filters)
    rescue_filters = ast.literal_eval(args.set_rescue_filters)
    with args.file:
        output = do_sv_wgs_filtering(args.file,
                                     frequency_filters=frequency_filters,
                                     rescue_filters=rescue_filters,
                                     interpretation_group=interpretation_group,
                                     caller_priority=args.caller_priority,
                                     output_format=args.output_format,
                                     debug=args.debug)

        print(*output, sep="\n")


if __name__ == "__main__":
    main()
