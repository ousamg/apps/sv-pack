#!/usr/bin/env python

"""sv_postprocessing.py: Script that updates the FILTER column
                         according to quality filters provided in
                         `analysistypeconfig.json` in `anno-targets`
"""

import sys
import json
import argparse
import logging
import ast
import itertools as it
from collections import OrderedDict

from svpack import sv_utils as utils

_log = logging.getLogger(__name__)


CALLERS = ["manta", "delly", "tiddit", "cnvnator"]


##########################################
# Parse command line input #
##########################################
def input_parser():
    parser = argparse.ArgumentParser(description='Postprocess VCF')
    parser.add_argument('file', nargs='?', type=argparse.FileType('r'),
                        default=sys.stdin, help='VCF filename')
    parser.add_argument('--set-quality-filters', type=str, default="{}",
                        help='JSON-formatted string to override: %(default)s')
    parser.add_argument('--callers', type=str, nargs="*", default=CALLERS,
                        help='Callers used to create VCF file %(default)s')
    return parser.parse_args()


###############################
# Adding missing header lines #
###############################
def add_headers_postprocess(known_callers):
    """
    Define the header lines to add in postprocess
    """
    hom_ref = dict(ID="HomRef",
                   Description='"homozygous reference call (filter applied at sample level)"')
    min_size = dict(ID="MinSize",
                    Description='"The variant is smaller than the user set limit"')
    low_dr = dict(ID="LowEvidence",
                  Description='"Too few discordant reads support event"')
    conflict_dr = dict(ID="EvidenceConflict",
                       Description='"Discordant reads evidence disagree with read depth for unbalanced event"')
    max_q0 = dict(ID="MaxQ0",
                  Description='"Too high fraction of MAPQ0-reads"')
    max_p = dict(ID="MaxP",
                 Description='"Too hight E-value P1 or P2"')

    added_headers = []
    if "delly" in known_callers:
        added_headers.extend([
            ("##FILTER=<ID=HomRef", hom_ref),
            ("##FILTER=<ID=MinSize", min_size),
            ("##FILTER=<ID=LowEvidence", low_dr),
            ("##FILTER=<ID=EvidenceConflict", conflict_dr),
        ])
    if "manta" in known_callers:
        added_headers.extend([
            ("##FILTER=<ID=LowEvidence", low_dr)
        ])
    if "tiddit" in known_callers:
        added_headers.extend([
            ("##FILTER=<ID=LowEvidence", low_dr),
            ("##FILTER=<ID=EvidenceConflict", conflict_dr),
        ])
    if "cnvnator" in known_callers:
        added_headers.extend([
            ("##FILTER=<ID=MinSize", min_size),
            ("##FILTER=<ID=MaxQ0", max_q0),
            ("##FILTER=<ID=MaxP", max_p),
        ])

    return added_headers


def complement_header(header,
                      added_headers):
    """
    Parse header.
    Optionally add new header definitions to output
    """
    parsed_header = map(utils.parse_header, header)
    return utils.complement_header(parsed_header,
                                   added_headers)


#############################
# VCF record postprocessing #
#############################
def _is_consistent_read_depth(svtype,
                              cn):
    """
    Check whether copy number estimate agrees with the svtype
    * Genotype is ignored
    """
    if all([svtype == "DEL", cn > 1]):
        return False
    elif all([svtype == "DUP", cn < 3]):
        return False
    else:
        return True


def _get_postprocessed_filter(record,
                              sample,
                              quality_filters):
    """
    Postprocess and return the FILTER column
    Set filters to user defined values (default from FILTERS)
    * Manta, Delly, Tiddit:
      add LowEvidence if few discordant reads
    * Delly, Tiddit:
      add EvidenceConflict if RD evidence does not support BND evidence
    * Delly, CNVnator:
      add MinSize if too short variant
    * Delly:
      add HomRef if GT=0/0
    * Tiddit:
      add RegionalQ if QUALA or QUALB are very low
    * CNVnator:
      add MapQ0 if large natorQ0
      add MapP if large natorP1 or natorP2
    """

    filters = OrderedDict.fromkeys(record["filter"].split(";"))

    # Postprocess FILTER according to order of priority
    if record["id"].startswith("Manta"):
        # Assert Manta specific ID

        svtype = record["info"]["SVTYPE"]
        pr_ref, pr_alt = map(int, sample["PR"].split(","))
        sr_ref, sr_alt = map(int, sample.get("SR", "0,0").split(","))  # Manta drops SR when "0,0"
        pr_range = quality_filters.get("manta", {}).get("PR")
        sr_range = quality_filters.get("manta", {}).get("SR")

        if sr_range:
            sr_min, sr_max = sr_range
            if svtype in ["INS"]:
                if sr_min <= sr_alt < sr_max:
                    filters.setdefault("LowEvidence")
        if pr_range:
            pr_min, pr_max = pr_range
            if svtype in ["DUP", "DEL"]:
                if pr_min <= pr_alt < pr_max:
                    filters.setdefault("LowEvidence")

    elif record["id"].startswith("SV_"):
        # Assert TIDDIT specific ID

        svtype = record["info"]["SVTYPE"]
        pr_alt = int(sample["DV"])
        sr_alt = int(sample["RV"])
        qual_a = int(record["info"]["QUALA"])
        qual_b = int(record["info"]["QUALB"])

        pr_range = quality_filters.get("tiddit", {}).get("DV")
        sr_range = quality_filters.get("tiddit", {}).get("RV")
        qual_a_range = quality_filters.get("tiddit", {}).get("QUALA")
        qual_b_range = quality_filters.get("tiddit", {}).get("QUALB")

        if sr_range:
            sr_min, sr_max = sr_range
            if svtype in ["INS"]:
                if sr_min <= sr_alt < sr_max:
                    filters.setdefault("LowEvidence")
        if pr_range:
            pr_min, pr_max = pr_range
            if svtype in ["DUP", "DEL"]:
                if pr_min <= pr_alt < pr_max:
                    filters.setdefault("LowEvidence")
        if qual_a_range:
            qual_a_min, qual_a_max = qual_a_range
            if qual_a_min <= qual_a < qual_a_max:
                filters.setdefault("RegionalQ")
        if qual_b_range:
            qual_b_min, qual_b_max = qual_b_range
            if qual_b_min <= qual_b < qual_b_max:
                filters.setdefault("RegionalQ")
        if sample["CN"].isdigit():
            cn = int(sample["CN"])
            if not _is_consistent_read_depth(svtype, cn):
                filters.setdefault("EvidenceConflict")

    elif record["id"].startswith(("DEL", "DUP", "INV", "BND", "INS")):
        # Assert Delly specific ID field

        svtype = record["info"]["SVTYPE"]
        pr_alt = int(sample["DV"])
        sr_alt = int(sample["RV"])
        sv_len = abs(int(record["info"]["SVLEN"]))
        gt = sample["GT"]

        pr_range = quality_filters.get("delly", {}).get("DV")
        sr_range = quality_filters.get("delly", {}).get("RV")
        sv_range = quality_filters.get("delly", {}).get("SVLEN")

        if sr_range:
            sr_min, sr_max = sr_range
            if svtype in ["INS"]:
                if sr_min <= sr_alt < sr_max:
                    filters.setdefault("LowEvidence")
        if pr_range:
            pr_min, pr_max = pr_range
            if svtype in ["DUP", "DEL"]:
                if pr_min <= pr_alt < pr_max:
                    filters.setdefault("LowEvidence")
        if sv_range:
            sv_len_min, sv_len_max = sv_range
            if sv_len_min <= sv_len < sv_len_max:
                filters.setdefault("MinSize")
        if gt in quality_filters.get("delly", {}).get("GT", []):
            filters.setdefault("HomRef")
        if sample["CN"].isdigit():
            cn = int(sample["CN"])
            if not _is_consistent_read_depth(svtype, cn):
                filters.setdefault("EvidenceConflict")

    elif record["id"].startswith("CNVnator"):
        # Assert CNVnator specific ID

        sv_len = abs(int(record["info"]["SVLEN"]))
        natorQ0 = float(record["info"]["natorQ0"])
        natorP1 = float(record["info"]["natorP1"])
        natorP2 = float(record["info"]["natorP2"])
        natorRD = float(record["info"]["natorRD"])

        sv_range = quality_filters.get("cnvnator", {}).get("SVLEN")
        q0_range = quality_filters.get("cnvnator", {}).get("natorQ0")
        p1_range = quality_filters.get("cnvnator", {}).get("natorP1")
        p2_range = quality_filters.get("cnvnator", {}).get("natorP2")

        if sv_range:
            sv_len_min, sv_len_max = sv_range
            if sv_len_min <= sv_len < sv_len_max:
                filters.setdefault("MinSize")
        HOM_DEL = 0.03
        if q0_range:
            q0_min, q0_max = q0_range
            if q0_min <= natorQ0 <= q0_max and natorRD > HOM_DEL:
                filters.setdefault("MaxQ0")
        if p1_range and p2_range:
            p1_min, p1_max = p1_range
            p2_min, p2_max = p2_range
            if p1_min <= natorP1 <= p1_max and p2_min <= natorP2 <= p2_max:
                filters.setdefault("MaxP")

    elif record["id"].startswith("DRAGEN"):
        # Assert Canvas specific ID from DRAGEN pipeline
        pass

    else:
        _log.warning("Unknown caller. No filtering rules implemented")

    if all([len(filters) > 1,
            "PASS" in filters]):
        filters.pop("PASS")

    FILTER = ";".join(filters)

    return FILTER


def _postprocess(record_std,
                 samples_std,
                 quality_filters={}):
    """
    Postprocess variant record
    * Update FILTER column
      if adding addional quality requirements
    """
    samples_filter, samples_std = it.tee(samples_std)
    filters = map(
        lambda sf: _get_postprocessed_filter(record_std, sf, quality_filters),
        samples_filter
    )
    record_std["filter"] = ";".join(filters)

    return (record_std, samples_std)


def postprocess(line,
                quality_filters={}):
    """
    Do FILTER postprocessing
    """

    (record_post, samples_post) = _postprocess(*utils.parse_record(line),
                                               quality_filters=quality_filters)

    return utils.format_record(record_post, samples_post)


def switch_header_body(is_header,
                       records,
                       quality_filters,
                       known_callers):
    """
    Switch between header lines and body lines
    """
    if is_header:
        return complement_header(records,
                                 add_headers_postprocess(known_callers))
    else:
        return map(lambda line: postprocess(line, quality_filters), records)


def do_sv_postprocessing(file_object,
                         quality_filters={},
                         known_callers=[]):
    """
    SV postprocessing does the following:
    * Add new filters to the VCF FILTER column
    """
    f_split = it.groupby(file_object, key=lambda line: line.startswith("#"))
    output_it = it.starmap(lambda is_header, records:
                           switch_header_body(is_header, records, quality_filters, known_callers),
                           f_split)
    return it.chain.from_iterable(output_it)


def main():
    logging.basicConfig(level=logging.INFO)

    args = input_parser()
    _log.info(f"Run parameters:\n{json.dumps(vars(args), indent=2, cls=utils.JSONArgparseEncoder)}")

    quality_filters = ast.literal_eval(args.set_quality_filters)
    with args.file:
        output = do_sv_postprocessing(args.file,
                                      quality_filters=quality_filters,
                                      known_callers=args.callers)

        print(*output, sep="\n")


if __name__ == "__main__":
    main()
